# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: AwesomeHaircut <jesusbalbastro at gmail com>
# Contributor: Mateusz Gozdek <mgozdekof@gmail.com>
# Contributor: Rein Fernhout <public@reinfernhout.xyz>
# Past Contributor: James An <james@jamesan.ca>

pkgbase=droidcam
pkgname=${pkgbase}-dev
pkgver=1.8.2
pkgrel=6.3
pkgdesc='A tool for using your android device as a wireless/usb webcam'
arch=('x86_64' 'aarch64')
url="https://github.com/dev47apps/${pkgbase}"
license=('GPL')
backup=("etc/modprobe.d/${pkgbase}.conf")
makedepends=('gtk3' 'libappindicator-gtk3' 'ffmpeg' 'libusbmuxd' 'imagemagick')
conflicts=("${pkgbase}" "${pkgbase}-dkms")
options=('!strip')
source=("${pkgbase}-${pkgver}.zip::${url}/archive/v${pkgver}.zip"
        "fix-audio.patch::https://github.com/dev47apps/droidcam/commit/9db911ee4f5a5ea75494d2e42342353470954296.patch"
        "${pkgbase}-modules.conf" "${pkgbase}-modprobe.conf"
        "https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}.desktop"
        "https://metainfo.manjariando.com.br/${pkgbase}/${pkgbase}.png")

sha512sums=('7b26dbb6757ef8911e3319a3ef944eae2691289cad98fb35ba872ee81e0760a592dd7f1c1735a17d783db43b696b6c58fe0c83923e7854bb54124922530fdec5'
            'ea7d5c29056db4c4d6acfb3ac33f35f263c65e74ca42c78ecfc9db62df4d5a1b458cb75bd4882bd8cfd173cb78b673c4e29459e315ada9676aea249850fb4d0f'
            '715be186aff2eca7b383ca5bef00e34d08363fd688cea49080828e8b31db3b43d9605d4e2951ffdece9e80a94645e065189617f87337b0084946f20f61f76588'
            'ed250836033eab59eabc81a1346168d886fbe026c56a43f04d0e153b2a9593d03be6846e114cde664fe00135a24b8bf55f46ece6ecee6aca52f651f29a3bb28b'
            '202692e87ee2331a83a6da8e9ba2b750552e115d9f0f5b0b9235bb5dc7082f439c82ec623e9f0b9655af6406bf140808cf5c1eefd952f51226a8202d3a672c43'
            'df0968e8da3b733e29cdda07f0f6f278d4be71d241b21d37631ed8c45b104fea718d05c395b306be8aba2c7acc1a9d69807c4ab9404812b684426ba6d879eea4'
            '8974a76f9d25e114fea8c91e954be3364a1f1d392f034919fc3a63bec46811f8b2bba3fd1eb1ac0de51622c5435a398792e615db23c4c5d9d04fc936aeae8090')

prepare() {
    cd "${srcdir}/${pkgbase}-${pkgver}"
    patch -Np1 -i "${srcdir}/fix-audio.patch"
}

build() {
    cd ${pkgbase}-${pkgver}

    # All JPEG* parameters are needed to use shared version of libturbojpeg instead of
    # static one.
    #
    # Also libusbmuxd requires an override while linking.
    make JPEG_DIR="" JPEG_INCLUDE="" JPEG_LIB="" JPEG=$(pkg-config --libs --cflags libturbojpeg) USBMUXD=-lusbmuxd-2.0
}

package() {
    depends=('alsa-lib' "v4l2loopback-dkms-git" 'ffmpeg' 'libjpeg-turbo' 'libusbmuxd' 'linux-headers' 'speex')
    optdepends=('gtk3: use GUI version in addition to CLI interface'
                'libappindicator-gtk3: use GUI version in addition to CLI interface')
    provides=("${pkgbase}=${pkgver}")

    pushd ${pkgbase}-${pkgver}

    # Install droidcam program files
    mkdir -p ${pkgdir}/opt/${pkgbase}
    install -Dm755 ${pkgbase} "${pkgdir}/opt/${pkgbase}/${pkgbase}"
    install -Dm755 ${pkgbase}-cli "${pkgdir}/opt/${pkgbase}/${pkgbase}-cli"
    install -Dm644 "${srcdir}/${pkgbase}-modprobe.conf" "${pkgdir}/etc/modprobe.d/${pkgbase}.conf"
    install -Dm644 "${srcdir}/${pkgbase}-modules.conf" "${pkgdir}/etc/modules-load.d/${pkgbase}.conf"
    install -Dm644 icon2.png "${pkgdir}/opt/${pkgbase}-icon.png"

    # Symlink
    mkdir -p ${pkgdir}/usr/bin
    ln -s /opt/${pkgbase}/${pkgbase} "${pkgdir}/usr/bin/${pkgbase}"
    ln -s /opt/${pkgbase}/${pkgbase}-cli "${pkgdir}/usr/bin/${pkgbase}-cli"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgbase}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgbase}.metainfo.xml"
    install -Dm644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgbase}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgbase}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgbase}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgbase}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgbase}.png"
    done
}

